const checkbox = document.getElementById("toggle_switch");
const plans = document.getElementsByClassName("pricing");

const checked = ["19.99", "24.99", "39.99"];

const unchecked = ["199.99", "249.99", "399.99"];

checkbox.addEventListener("change", () => {
  const prices = checkbox.checked ? checked : unchecked;
  for (let plan = 0; plan < plans.length; plan++) {
    plans[plan].innerHTML = prices[plan];
  }
});
